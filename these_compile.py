#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Compile des fichiers de thèse

Usage: depuis le dossier de thèse, `these_compile.py filename [out]` compile
le fichier filename (les numéros de chapitre suffisent), et écrit un fichier
odt en sortie dans le fichier out (par défaut dans
"Fichers odt/[filename].odt").
"""

import sys
import os
import re
import csv
import tempfile
import subprocess
from subprocess import Popen, PIPE  # To pipe commands together.
from shutil import which  # To check if command exists in PATH.

scr_path = sys.path[0]
lua_filter = os.path.join(scr_path, 'these_filter.lua')

# TODO: Keep this consistent with lua filter...
start_index = "START_INDEX"
end_index = "END_INDEX"

# Réglages perso pour pandoc.
toc_depth = 2
biblio = '/home/aurore/Bibtex/engagees.json'
csl = '/home/aurore/Zotero/styles/perso-zettlr-these.csl'
ref_doc = '/home/aurore/Dropbox/Documents Aurore/Thèse/Rédaction/Pandoc/custom-reference.odt'

# TODO: Keep this consistent with the file cles-index.csv
cle_colname = 'clé'
index_colname = 'index'

def escape_quote_marks(text):
    return re.sub('"', r'\"', text)

# Importe les infos du fichier cles-index.csv
cles_index = os.path.join(scr_path, 'cles-index.csv')
if os.path.exists(cles_index):
    with open(cles_index, newline='') as file:
        # Find the "good" delimiter.
        for delim in [';', '\t', ',']:
            file.seek(0)
            row = file.readline().split(delim)
            if len(row) >= 2:
                file.seek(0)
                break
        # TODO: search good quotechar?
        quote = '"'
        reader = csv.reader(file, delimiter=delim, quotechar=quote)
        # We use the first row to identify columns.
        row = next(reader)
        cols = {k: i for i, k in enumerate(row)}
        # Two columns must be here: 'index' and 'clé'
        if index_colname not in cols or cle_colname not in cols:
            print('Warning: Found file "cles-index.csv" but important columns '
                  'are missing. Ignoring this file.', file=sys.stderr)
            cles_index = None
        else:
            cle_col = cols.pop(cle_colname)
            index_col = cols.pop(index_colname)
            cles_index = {}
            for row in reader:
                cle = row[cle_col]
                if cle in cles_index:
                    print(f'Warning: Found several keys "{cle}" in file'
                          ' "cles-index.csv". Ignoring.', file=sys.stderr)
                    continue
                index = escape_quote_marks(row[index_col])
                text = ' '.join([f'"{index}"'] \
                                + [f'{col}="{escape_quote_marks(row[i])}"'
                                   for col, i in cols.items()
                                   if row[i] != ''])
                cles_index[cle] = text

# Regex to match 'key=value' or 'key="Some value with spaces"'.
re_key_value = re.compile(r'(?P<key>\w+)=(?P<value>"([^"]*(\\")?)*(?<!\\)"|\w+)')

def parse_custom_text(text, nline, span):
    ctype = None  # Content type: index, ref or renvoi.
    d = set()
    new_text = '<'
    for match in re_key_value.finditer(text):
        key = match.group('key')
        val = match.group('value')
        if ctype is None:
            if key in ['index', 'ref', 'renvoi']:
                ctype = key
            else:
                raise Exception('Could not parse index or ref or renvoi in '
                                f'content.xml at line {nline}:{span}.')
        if key in d:
            print(f'Warning: found several "{key}" keys in content.xml at '
                  f'line {nline}:{span}. Taking the first one encountered.'
                  f' Ignored value {val}.',
                  file=sys.stderr)
            continue
        d.add(key)
        # Adapt key to be written.
        if key == 'index':
            new_text += 'text:alphabetical-index-mark'
            key = 'text:string-value'
            # Special treatment for index.
            if val.strip('"') in cles_index:
                val = cles_index[val.strip('"')]
                new_text += f' {key}={val}'
                break  # Other keys are ignored in this case.
        elif key == 'ref':
            new_text += 'text:bookmark'
            key = 'text:name'
        elif key == 'renvoi':
            new_text += 'text:bookmark-ref text:reference-format="page"'
            key = 'text:ref-name'
        else:
            key = f'text:{key}'
        # Adapt value: add quotes.
        if val[0] != '"':
            val = f'"{val}"'
        new_text += f' {key}={val}'
    # Special ending for renvoi.
    if ctype == 'renvoi':
        new_text += '>0</text:bookmark-ref>'
    else:
        new_text += '/>'
    return new_text

def process_content(file, out):
    re_index = re.compile(f'{start_index}(.*?){end_index}')
    with open(file) as file, open(out, 'w') as out:
        for nline, line in enumerate(file):
            new_line = ''
            i=0
            for match in re_index.finditer(line):
                content = re.sub('&quot;', '"', match.group(1))
                (j, k) = match.span()
                new_content = parse_custom_text(content, nline, (j,k))
                new_line += f'{line[i:j]}{new_content}'
                i = k
            new_line += line[i:]
            out.write(new_line)

if __name__ == '__main__':
    if len(sys.argv) == 1:
        raise Exception('Erreur: donner un fichier en argument.')
    
    file = sys.argv[1]
    dirname = os.path.dirname(file)
    if dirname == '':
        dirname = '.'
    filename = os.path.basename(file)
    number = re.match(r'\d+\.?( \d+\.?)?', filename)
    has_notes = False
    if number:
        number = number.group()
        if number[-1] != '.':
            number += '.'
        
        l = [f for f in os.listdir(dirname) if re.search(f'^{number}', f)]
        if len(l) == 0:
            print("Pas de fichier avec ce nom.")
            sys.exit(1)
        elif len(l) == 2:
            # Identifie le fichier de notes.
            has_notes = True
            a, b = l
            if re.search(r'Notes\.(md|txt)$', a):
                note_file = os.path.join(dirname, a)
                l = [b]
            elif re.search(r'Notes\.(md|txt)$', b):
                note_file = os.path.join(dirname, b)
            else:
                raise Exception(f'Erreur: deux fichiers trouvés: {l}'
                                ' mais pas de notes identifiées.')
        elif len(l) != 1:
            raise Exception(f'Erreur: trop de fichiers: {l}')
        
        if filename != l[0]:
            file = os.path.join(dirname, l[0])
            filename = l[0]
    
    # If a second argument is present, it is taken as the output file.
    # Note that outfile needs to be an absolute path (for the zip cmd later).
    name = re.sub(r'\.\w*$', r'', file)
    if len(sys.argv) > 2:
        outdir = None
        outfile = os.path.abspath(sys.argv[2])
    else:
        outdir = os.path.join(os.getcwd(), 'Fichiers odt')
        outfile = os.path.join(outdir, f'{name}.odt')
    
    # Create outdir if it does not exist.
    if outdir is not None:
        if os.path.exists(outdir):
            if not os.path.isdir(outdir):
                raise Exception(f'Erreur: "{outdir}" est un fichier.')
        else:
            os.mkdir(outdir)
    
    tmp_dir = tempfile.TemporaryDirectory()
    tmp_content = os.path.join(tmp_dir.name, 'content.xml')
    unzip_dir = os.path.join(tmp_dir.name, 'unzip')
    
    # Pipe the first part together:
    # 1. Read the files.
    cat_cmd = ['cat', file]
    if has_notes:
        cat_cmd.append(note_file)
    # 2. Turn links into spans with sed.
    sed_cmd = ['sed', '-E', r's/\[([^]]+)\]\(((index|ref|renvoi)="[^"]+"(\s[^\s()"]+="[^"]+")*)\)/[\1]{\2}/g']
    # 3. Pandoc with custom filter
    pandoc_cmd = ['pandoc', '-f', 'markdown', '-t', 'odt',
                  f'--lua-filter={lua_filter}',
                  '--toc', f'--toc-depth={toc_depth}',
                  f'--bibliography={biblio}',
                  f'--csl={csl}',
                  '-o', outfile,
                  f'--reference-doc={ref_doc}']
    # # For simple tests. TODO: delete this
    # pandoc_cmd = ['pandoc', '-f', 'markdown', '-t', 'odt',
    #               f'--lua-filter={lua_filter}',
    #               '--toc', f'--toc-depth={toc_depth}',
    #               '-o', outfile]
    # ########################################
    
    cat_proc = Popen(cat_cmd, stdout=PIPE)
    sed_proc = Popen(sed_cmd, stdin=cat_proc.stdout, stdout=PIPE)
    cat_proc.stdout.close()  # Recommanded by the doc...
    pandoc_proc = Popen(pandoc_cmd, stdin=sed_proc.stdout)
    sed_proc.stdout.close()
    pandoc_proc.wait()
    
    # Now unzip file and make changes.
    subprocess.run(['unzip', '-q', outfile, '-d', unzip_dir])
    process_content(os.path.join(unzip_dir, 'content.xml'), tmp_content)
    
    # Replace old content by new content and rezip.
    os.rename(tmp_content, os.path.join(unzip_dir, 'content.xml'))
    cwd = os.getcwd()
    os.chdir(unzip_dir)
    subprocess.run(['zip', '-q', outfile, '*'])
    os.chdir(cwd)
    
    # Removes temp files.
    tmp_dir.cleanup()
    
    # Finally, try to open outfile if LibreOffice is installed.
    soffice = which('soffice')
    if soffice is not None:
        os.spawnl(os.P_NOWAIT, soffice, soffice, outfile)
