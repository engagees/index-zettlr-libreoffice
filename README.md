# Compilateur de thèse en markdown

Attention: rien n'est garanti. Ne fonctionnera sans doute que sous Linux (et même peut-être
seulement Debian-based). Faites des copies de vos documents de travail avant d'utiliser cet
outil.

## TODO

- [ ] Compléter ce README
- [x] `install.sh`.
- [ ] Améliorer `install.sh` (détection si tout est installé)
- [x] Prendre en compte un deuxième argument pour fichier de sortie.
- [x] Ouvrir le doc à la fin automatiquement.
- [x] Prendre en compte le fichier cles-index.csv.
- [ ] ? Un fichier de configuration.
- [ ] ? Un mode debug / verbose.

## Dépendances

- [Git](https://www.git-scm.com) pour cloner ce dépôt.
- [Pandoc](https://pandoc.org)
- Python 3
- Utilitaires communs: `sed`, `zip` et `unzip` (probablement déjà installés, sinon utiliser
  `sudo apt install [program(s)]` en ligne de commande pour installer l'un ou l'autre des 
  commandes cités)
- (optionnel) LibreOffice pour lire le fichier de sortie

## Installation (Linux)

D'abord, cloner ce dépôt dans un dossier destiné à perdurer (par exemple dans
`~/.these_compile/`). Si toutes les dépendances sont là, le script `install.sh`
créera simplement un lien symbolique `~/.local/bin/these_compile` vers le script `these_compile.py`.

Pour mettre à jour, utiliser `git pull` devrait suffire, à moins de trop gros changements
futurs.

### TODO?

- [ ] un script de désinstallation
- [ ] installation automatique des dépendances

## Utilisation

Depuis la ligne de commande dans un dossier contenant un fichier de thèse (i.e.
`1. 1. Intro.md`), entrer `these_compile "1. 1" [sortie]` pour traduire le fichier
au format odt dans le fichier `sortie` (par défaut `./Fichiers odt/[nom du fichier].odt`)

## Licence

- [ ] TODO: ajouter des fichiers pour une licence libre adaptée (GNU GPL 3...)
