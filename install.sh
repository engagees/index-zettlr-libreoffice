#!/bin/sh

ln -s "$(pwd)/these_compile.py" ~/bin/these_compile

echo "The script these_compile.py has been installed. To uninstall please" \
     "remove\nthe link \"~/.local/bin/these_compile\"." \
     "\nInstall python, pandoc, sed, zip and unzip if they are not" \
     "installed on\nyour system yet."
