-- TODO: Keep this consistent with python script...
start_index = "START_INDEX"
end_index = "END_INDEX"

function Span (span)
  if span.attributes['index']
     or span.attributes['renvoi']
     or span.attributes['ref'] then
    local text = start_index
    for k, v in pairs(span.attributes) do
        text = text .. string.format(' %s="%s"', k, v)
    end
    text = text .. end_index
    if span.attributes['renvoi'] then
      return {pandoc.Str(text)}
      end
    return span.content .. {pandoc.Str(text)}
  end
end

